package me.github.fwfuratdo.ipay.authorization.login;

import me.github.fwfuratdo.ipay.authorization.domain.SystemUser;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class SystemUserConverter implements Converter<SystemUser, ResourceOwner> {
    @Override
    public ResourceOwner convert(SystemUser source) {
        return new ResourceOwner(source.getEmail(), source.getPassword());
    }
}
