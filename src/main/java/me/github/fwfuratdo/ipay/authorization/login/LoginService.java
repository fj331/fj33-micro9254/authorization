package me.github.fwfuratdo.ipay.authorization.login;

import me.github.fwfuratdo.ipay.authorization.domain.SystemUser;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class LoginService implements UserDetailsService {

    private final UserClient userClient;
    private final Converter<SystemUser, ResourceOwner> converter;

    public LoginService(UserClient userClient, Converter<SystemUser, ResourceOwner> converter) {
        this.userClient = userClient;
        this.converter = converter;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("LoginService");

        var systemUser = userClient.findUserByUsername(username);
        var resourceOwner = converter.convert(systemUser);

        return resourceOwner;
    }
}
