package me.github.fwfuratdo.ipay.authorization.login;

import feign.hystrix.FallbackFactory;
import me.github.fwfuratdo.ipay.authorization.domain.SystemUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "users", fallbackFactory = UserClient.UserClientFallbackFactory.class)
public interface UserClient {

    @GetMapping("filter")
    SystemUser findUserByUsername(@RequestParam String email);


    @Component
    class UserClientFallbackFactory implements FallbackFactory<UserClient> {

        @Override
        public UserClient create(Throwable cause) {
            System.out.println(cause);
            
            throw new UsernameNotFoundException("Username/Password invalid");
        }
    }

}
